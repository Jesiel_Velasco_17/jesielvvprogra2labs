package prograiilabs;

/**
 * This is the App class.
 */
public class App {
  public static void main(String[] args) {
    System.out.println(new App().getGreeting());
  }

  public String getGreeting() {
    return "Hello World!";
  }
}
