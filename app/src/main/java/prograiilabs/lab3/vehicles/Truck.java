package prograiilabs.lab3.vehicles;

/**
 * This is the Truck Class (which inherits from Vehicle).
 */
public class Truck extends Vehicle {
  public Truck(String brand, String model, int year, double mileage) {
    super(brand, model, year, mileage);
  }

  @Override
  public boolean needsMaintenance() {
    return true;
  }
}
