package prograiilabs.lab3.vehicles;

/**
 * This is the Car Class (which inherits from Vehicle).
 */
public class Car extends Vehicle {
  public Car(String brand, String model, int year, double mileage) {
    super(brand, model, year, mileage);
  }

  @Override
  public boolean needsMaintenance() {
    return false;
  }
}