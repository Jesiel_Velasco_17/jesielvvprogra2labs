package prograiilabs.lab3.vehicles;

/**
 * This is the Vehicle Status Interface.
 */
interface VehicleState {
  boolean needsMaintenance();
}
