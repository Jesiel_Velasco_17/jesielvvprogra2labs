package prograiilabs.lab3.vehicles;

import prograiilabs.lab3.customer.Comment;

/**
 * This is the abstract class Vehicle.
 */
public abstract class Vehicle {
  private final String brand;
  private final String model;
  private final int year;
  private final double mileage;
  private Comment vehicleComment;
  private boolean needsMaintenance;

  /**
   * This is the class Vehicle.
   */
  public Vehicle(String brand, String model, int year, double mileage) {
    this.brand = brand;
    this.model = model;
    this.year = year;
    this.mileage = mileage;
    this.needsMaintenance = false;
  }

  public double getMileage() {
    return mileage;
  }

  public abstract boolean needsMaintenance();

  public Comment getComment() {
    return vehicleComment;
  }

  public void setNeedsMaintenance(boolean needsMaintenance) {
    this.needsMaintenance = needsMaintenance;
  }

  public void setComment(Comment comment) {
    this.vehicleComment = comment;
  }
}
