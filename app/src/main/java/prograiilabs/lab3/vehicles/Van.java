package prograiilabs.lab3.vehicles;

/**
 * This is the Van Class (which inherits from Vehicle).
 */
public class Van extends Vehicle {
  public Van(String brand, String model, int year, double mileage) {
    super(brand, model, year, mileage);
  }

  @Override
  public boolean needsMaintenance() {
    return getMileage() > 100000;
  }
}
