
package prograiilabs.lab3.customer;

import prograiilabs.lab3.vehicles.Vehicle;

/**
 * This is the Client Class.
 */
public class Customer {

  private final String customerName;
  private final String customerLastName;
  private final int customerCellPhoneNumber;
  private final int customerLicenseNumber;
  private Vehicle vehicle;

  /**
   * Customer.
   */
  public Customer(String customerName, String customerLastName, int customerCellPhoneNumber,
      int customerLicenseNumber) {
    this.customerName = customerName;
    this.customerLastName = customerLastName;
    this.customerCellPhoneNumber = customerCellPhoneNumber;
    this.customerLicenseNumber = customerLicenseNumber;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle car2) {
    this.vehicle = car2;
  }
}