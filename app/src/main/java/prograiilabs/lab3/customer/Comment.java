package prograiilabs.lab3.customer;

/**
 * This is the Comment class.
 */
public class Comment {
  private String comment;
  private String text;

  public Comment(String comment) {
    this.comment = comment;
    this.text = text;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getText() {
    return text;
  }
}
