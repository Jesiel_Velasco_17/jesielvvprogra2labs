package prograiilabs.lab3;

import prograiilabs.lab3.customer.Comment;
import prograiilabs.lab3.customer.Customer;
import prograiilabs.lab3.rent.Prices;
import prograiilabs.lab3.rent.Rent;
import prograiilabs.lab3.vehicles.Car;
import prograiilabs.lab3.vehicles.Truck;
import prograiilabs.lab3.vehicles.Van;
import prograiilabs.lab3.vehicles.Vehicle;

/**
 * This is the Main class to test the system.
 */
public class Main {

  /**
   * This is the Main.
   */
  public static void main(String[] args) {

    Vehicle car1 = new Car("BMW", "X6", 2022, 40000);
    Vehicle car2 = new Car("Audi", "R8", 2018, 20450);
    Vehicle truck1 = new Truck("Volvo", "FH16", 2018, 200000);
    Vehicle van1 = new Van("Volkswagen", "Crafter", 1997, 80000);

    Customer customer1 = new Customer("Daniel ", "Rivera", 73750296, 587123);
    Customer customer2 = new Customer("Fernando", "Lopez", 65291350, 928789);

    customer2.setVehicle(car2);
    Rent rent1 = new Rent(customer1, 7, Prices.POR_WEEK.getWorth(), true);
    rent1.addAdditionalService("GPS");
    rent1.addAdditionalService("Wifi");

    Rent rent2 = new Rent(customer1, 7, Prices.POR_WEEK.getWorth(), true);
    rent2.addAdditionalService("GPS");
    rent2.addAdditionalService("Wifi");

    Comment comment1 = new Comment("Excellent service, vehicle in perfect condition.");
    rent1.setComment(comment1);

    Rent rent3 = new Rent(customer2, 5, Prices.POR_DAY.getWorth(), false);
    rent3.addAdditionalService("GPS");

    if (rent3.canRent()) {
      System.out.println("The vehicle can be rented.");
    } else {
      System.out.println("The vehicle needs maintenance and cannot be rented at this time.");
    }
  }
}
