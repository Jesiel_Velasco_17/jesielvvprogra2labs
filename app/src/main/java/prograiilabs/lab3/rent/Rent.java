package prograiilabs.lab3.rent;

import java.util.ArrayList;
import java.util.List;
import prograiilabs.lab3.customer.Comment;
import prograiilabs.lab3.customer.Customer;

/**
 * This is the Rental Class.
 */
public class Rent {
  private final boolean sure;
  private Customer customer;
  private int duration;
  private double price;
  private List<String> additionalServices;
  private Comment comment;

  /**
   * Rent.
   */
  public Rent(Customer customer, int duration, double price, boolean sure) {
    this.customer = customer;
    this.duration = duration;
    this.price = price;
    this.sure = sure;
    this.additionalServices = new ArrayList<>();
  }

  /**
   * This is the getPriceWithDiscount.
   */
  public double getPriceWithDiscount() {
    if (duration >= 7) {
      return price - (price * 0.10);
    } else {
      return price;
    }
  }

  public boolean canRent() {
    return !customer.getVehicle().needsMaintenance();
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public Comment getComment() {
    return comment;
  }

  public void setComment(Comment comment) {
    this.comment = comment;
  }

  public List<String> getAdditionalServices() {
    return additionalServices;
  }

  public void setAdditionalServices(List<String> additionalServices) {
    this.additionalServices = additionalServices;
  }

  public void addAdditionalService(String service) {
    additionalServices.add(service);
  }
}
