package prograiilabs.lab3.rent;

/**
 * This is the Price List.
 */
public enum Prices {
  POR_HOUR(3),
  POR_DAY(9),
  POR_WEEK(16);

  private final double worth;

  Prices(double worth) {
    this.worth = worth;
  }

  public double getWorth() {
    return worth;
  }
}
