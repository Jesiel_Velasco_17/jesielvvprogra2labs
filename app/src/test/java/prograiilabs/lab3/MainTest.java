package prograiilabs.lab3;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import prograiilabs.lab3.customer.Comment;
import prograiilabs.lab3.customer.Customer;
import prograiilabs.lab3.rent.Prices;
import prograiilabs.lab3.rent.Rent;
import prograiilabs.lab3.vehicles.Car;
import prograiilabs.lab3.vehicles.Vehicle;

/**
 * This is the MainTest.
 */
public class MainTest {

  @Test
  public void testCanRentWhenVehicleNeedsMaintenance() {
    Vehicle vehicle = new Car("BMW", "X6", 2022, 40000);
    vehicle.setComment(new Comment("Needs maintenance"));
    Customer customer = new Customer("Daniel", "Rivera", 73750296, 587123);
    customer.setVehicle(vehicle);
    Rent rent = new Rent(customer, 7, Prices.POR_WEEK.getWorth(), true);

    boolean canRent = rent.canRent();

    assertTrue(canRent);
  }

  @Test
  public void testCanRentWhenVehicleDoesNotNeedMaintenance() {
    Vehicle vehicle = new Car("Audi", "R8", 2018, 20450);
    Customer customer = new Customer("Fernando", "Lopez", 65291350, 928789);
    customer.setVehicle(vehicle);
    Rent rent = new Rent(customer, 7, Prices.POR_WEEK.getWorth(), true);

    boolean canRent = rent.canRent();

    assertTrue(canRent);
  }
}
