package prograiilabs.lab3.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import prograiilabs.lab3.vehicles.Car;
import prograiilabs.lab3.vehicles.Truck;
import prograiilabs.lab3.vehicles.Vehicle;

/**
 * This is the CustomerTest.
 */
public class CustomerTest {

  @Test
  public void testGetVehicle() {
    Vehicle expectedVehicle = new Car("BMW", "X6", 2022, 40000);
    Customer customer = new Customer("Daniel", "Rivera", 73750296, 587123);
    customer.setVehicle(expectedVehicle);

    Vehicle actualVehicle = customer.getVehicle();

    assertEquals(expectedVehicle, actualVehicle);
  }

  @Test
  public void testSetVehicle() {
    Customer customer = new Customer("Fernando", "Lopez", 65291350, 928789);
    Vehicle newVehicle = new Truck("Volvo", "FH16", 2018, 200000);

    customer.setVehicle(newVehicle);

    assertEquals(newVehicle, customer.getVehicle());
  }
}
