package prograiilabs.lab3.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * This is the CommentTest.
 */
public class CommentTest {

  @Test
  public void testGetComment() {
    String expectedComment = "This is a test comment";
    Comment comment = new Comment(expectedComment);

    String actualComment = comment.getComment();

    assertEquals(expectedComment, actualComment);
  }

  @Test
  public void testSetComment() {
    Comment comment = new Comment("Initial comment");
    String newComment = "Updated comment";

    comment.setComment(newComment);

    assertEquals(newComment, comment.getComment());
  }
}
