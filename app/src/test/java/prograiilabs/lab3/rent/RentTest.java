package prograiilabs.lab3.rent;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import prograiilabs.lab3.customer.Comment;
import prograiilabs.lab3.customer.Customer;
import prograiilabs.lab3.vehicles.Vehicle;

/**
 * This is the RentTest.
 */
public class RentTest {
  private Customer customer;
  private Rent rent;

  @BeforeEach
  public void setUp() {
    customer = new Customer("Daniel ", "Rivera", 73750296, 587123);
  }

  @Test
  public void testGetPriceWithDiscount() {
    rent = new Rent(customer, 10, 100.0, true);
    assertEquals(90.0, rent.getPriceWithDiscount(), 0.001);

    rent = new Rent(customer, 5, 100.0, true);
    assertEquals(100.0, rent.getPriceWithDiscount(), 0.001);
  }

  @Test
  public void testCanRent() {
    Vehicle vehicle = new Vehicle("BMW", "X6", 2022, 40000) {
      @Override
      public boolean needsMaintenance() {
        return false;
      }
    };
    customer.setVehicle(vehicle);

    rent = new Rent(customer, 7, 150.0, true);
    assertTrue(rent.canRent());

    rent = new Rent(customer, 3, 150.0, true);
    assertTrue(rent.canRent());

    Vehicle vehicleMaintenanceNeeded = new Vehicle("Audi", "R8", 2018, 20450) {
      @Override
      public boolean needsMaintenance() {
        return false;
      }
    };
    customer.setVehicle(vehicleMaintenanceNeeded);

    rent = new Rent(customer, 7, 150.0, true);
    assertTrue(rent.canRent());
  }

  @Test
  public void testAddAdditionalService() {
    rent = new Rent(customer, 2, 50.0, true);

    rent.addAdditionalService("GPS");
    rent.addAdditionalService("Child Seat");

    assertEquals(2, rent.getAdditionalServices().size());
    assertTrue(rent.getAdditionalServices().contains("GPS"));
    assertTrue(rent.getAdditionalServices().contains("Child Seat"));
  }

  @Test
  public void testComment() {
    Comment comment = new Comment("Excellent service, vehicle in perfect condition.");
    rent = new Rent(customer, 5, 200.0, true);

    assertNull(rent.getComment());

    rent.setComment(comment);

    assertNotNull(rent.getComment());
    assertEquals(null, rent.getComment().getText());
  }
}
