package prograiilabs.lab3.rent;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * This is the PricesTest.
 */
public class PricesTest {

  @Test
  public void testPricesValues() {
    double porHourValue = Prices.POR_HOUR.getWorth();
    double porDayValue = Prices.POR_DAY.getWorth();
    double porWeekValue = Prices.POR_WEEK.getWorth();

    assertEquals(3.0, porHourValue, 0.01);
    assertEquals(9.0, porDayValue, 0.01);
    assertEquals(16.0, porWeekValue, 0.01);
  }
}
