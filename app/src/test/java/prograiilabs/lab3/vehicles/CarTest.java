package prograiilabs.lab3.vehicles;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

/**
 * This is the CarTest.
 */
public class CarTest {

  @Test
  public void testNeedsMaintenance() {
    Car car = new Car("BMW", "X6", 2022, 40000);
    boolean needsMaintenance = car.needsMaintenance();
    assertFalse(needsMaintenance);
  }
}