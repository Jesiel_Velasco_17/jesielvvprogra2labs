package prograiilabs.lab3.vehicles;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

/**
 * This is the VanTest.
 */
public class VanTest {

  @Test
  public void testNeedsMaintenance() {
    Van van = new Van("Volkswagen", "Crafter", 1997, 80000);
    boolean needsMaintenance = van.needsMaintenance();
    assertFalse(needsMaintenance);
  }
}
