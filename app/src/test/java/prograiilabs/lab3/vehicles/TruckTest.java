package prograiilabs.lab3.vehicles;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * This is the TruckTest.
 */
public class TruckTest {
  @Test
  public void testNeedsMaintenance() {
    Truck truck = new Truck("Volvo", "FH16", 2018, 200000);
    boolean needsMaintenance = truck.needsMaintenance();
    assertTrue(needsMaintenance);
  }
}