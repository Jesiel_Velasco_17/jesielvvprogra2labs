package prograiilabs.lab3.vehicles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import prograiilabs.lab3.customer.Comment;

/**
 * This is the VehicleTest.
 */
public class VehicleTest {

  @Test
  public void testGetMileage() {
    double expectedMileage = 40000;
    Vehicle vehicle = new Car("BMW", "X6", 2022, expectedMileage);

    double actualMileage = vehicle.getMileage();

    assertEquals(expectedMileage, actualMileage, 0.01);
  }

  @Test
  public void testSetAndGetComment() {
    Vehicle vehicle = new Car("BMW", "X6", 2022, 40000);
    Comment comment = new Comment("This is a comment about the vehicle.");
    vehicle.setComment(comment);
    Comment actualComment = vehicle.getComment();
    assertEquals(comment, actualComment);
  }

  @Test
  public void testNeedsMaintenance() {
    Vehicle vehicle = new Car("BMW", "X6", 2022, 40000);
    boolean needsMaintenance = vehicle.needsMaintenance();
    assertFalse(needsMaintenance);
  }
}
